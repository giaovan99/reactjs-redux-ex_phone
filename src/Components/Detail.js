import React, { Component } from "react";

export default class Detail extends Component {
  render() {
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.detail;
    return (
      <div className="row">
        <div className="col-4">
          <img src={hinhAnh} />
        </div>
        <div className="col-8">
          <h2>{tenSP}</h2>
          <p>{manHinh}</p>
          <p>{heDieuHanh}</p>
          <p>{cameraTruoc}</p>
          <p>{cameraSau}</p>
          <p>{ram}</p>
          <p>{rom}</p>
          <p>{giaBan}</p>
        </div>
      </div>
    );
  }
}
