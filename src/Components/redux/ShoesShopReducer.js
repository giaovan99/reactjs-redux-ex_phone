import { data } from "../data";
const stateShoesShop = {
  listShoe: data,
  detail: [],
};
export const ShoesShopReducer = (state = stateShoesShop, action) => {
  switch (action.type) {
    case "CHI_TIET": {
      state.detail = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
