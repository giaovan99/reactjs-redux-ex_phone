import { combineReducers } from "redux";
import { ShoesShopReducer } from "./ShoesShopReducer";
export const rootReducer = combineReducers({
  ShoesShopReducer,
});
