import React, { Component } from "react";
import { connect } from "react-redux";
import { data } from "./data";
import Detail from "./Detail";
import List from "./List";

class Phone extends Component {
  renderDetail = (phone) => {
    let clone = phone;
    this.setState({
      detail: clone,
    });
  };
  render() {
    console.log("props", this.props);
    return (
      <div>
        <List
          list={this.props.listPhone}
          renderDetail={this.props.handleChiTiet}
        />
        <Detail detail={this.props.detail} />
      </div>
    );
  }
}

let mapStatetoProps = (state) => {
  return {
    listPhone: state.ShoesShopReducer.listShoe,
    detail: state.ShoesShopReducer.detail,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChiTiet: (item) => {
      let action = {
        type: "CHI_TIET",
        payload: item,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStatetoProps, mapDispatchToProps)(Phone);
