import React, { Component } from "react";
import Item from "./Item";

export default class List extends Component {
  renderListPhone = () => {
    return this.props.list.map((item) => {
      return <Item phone={item} renderDetail={this.props.renderDetail} />;
    });
  };
  render() {
    return <div className="row">{this.renderListPhone()}</div>;
  }
}
